public class Point {
	
	private int xCoord;
	private int yCoord;
	
	public Point(int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}
	
	public double distanceFromPoint(Point point) {
		int xDiff = xCoord - point.getxCoord();
		int yDiff = yCoord - point.getyCoord();
		
		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}


}
